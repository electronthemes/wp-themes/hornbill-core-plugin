<?php
/*
    Plugin Name: Hornbill Core
    Description: The driving fource of hornbill theme
*/

//require dirname(__FILE__) . '/metaboxes/index.php';
//add_filter('acf/settings/show_admin', '__return_false'); // disable acf menu

require dirname(__FILE__) . '/widgets/index.php';
require dirname(__FILE__) . '/elementor/hornbill-elementor-widgets.php';