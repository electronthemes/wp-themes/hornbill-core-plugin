<?php
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5bae68a1a7dab',
        'title' => 'Navigation Setting',
        'fields' => array(
            array(
                'key' => 'field_5bae68b0fc130',
                'label' => 'Navigation Type',
                'name' => 'navigation_type',
                'type' => 'radio',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'normal' => 'Normal',
                    'fixed_top' => 'Fixed Top',
                ),
                'allow_null' => 0,
                'other_choice' => 0,
                'default_value' => 'fixed_top',
                'layout' => 'vertical',
                'return_format' => 'value',
                'save_other_choice' => 0,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;