<?php
add_action('admin_enqueue_scripts', function(){
    wp_enqueue_script( 'media-upload' );
    wp_enqueue_media();
    wp_enqueue_script('upload_media_widget', plugin_dir_url(__FILE__) . 'upload-media.js');
});

class Hornbill_Author_info_widget extends WP_Widget {

	public function __construct() {
		$widget_ops = array( 
			'classname' => 'Hornbill_Author_info_widget',
		);
		parent::__construct( 'Hornbill_Author_info_widget', 'Hornbill: Author Info', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) { 
    ?>
    <aside class="widget m-bot-50 m-sm-bot-30">
        <div class="widget-author">
            <div class="author-cover bg-victoria"></div>

            <?php if($instance['photo']): ?>
            <div class="auth-avatar">
                <img src="<?php echo $instance['photo']; ?>" alt="<?php echo $instance['name']; ?>">
            </div>
            <?php endif; ?>

            <div class="auth-contents">
                <h4 class="auth-title"><?php echo $instance['name']; ?></h4>
                <h6 class="auth-cat"><?php   echo $instance['designation']; ?></h6>
                <div class="m-top-20">
                    <p><?php echo $instance['bio']; ?></p>
                </div>
            </div>
        </div>
    </aside>
	<?php }
	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) { ?>
		<p>
            <?php $name = 'title';  ?>
			<label for="<?php echo $this->get_field_id($name); ?>"><?php echo esc_html__( ucfirst($name) ,'hornbill') ?></label>
			<input 
                type="text" 
                name="<?php echo $this->get_field_name($name); ?>" 
                id="<?php echo $this->get_field_id($name); ?>" 
                value="<?php echo isset($instance[$name]) ? $instance[$name] : ''; ?>" 
                class="widefat">
		</p>

        <p>
            <?php 
                $name = 'name';  
                $title = 'Author Name';  
            ?>
            <label for="<?php echo $this->get_field_id($name); ?>"><?php echo esc_html__( ucfirst($title) ,'hornbill') ?></label>
            <input 
                type="text" 
                name="<?php echo $this->get_field_name($name); ?>" 
                id="<?php echo $this->get_field_id($name); ?>" 
                value="<?php echo isset($instance[$name]) ? $instance[$name] : ''; ?>" 
                class="widefat">
        </p>


        <p>
            <?php $name = 'designation';  ?>
            <label for="<?php echo $this->get_field_id($name); ?>"><?php echo esc_html__( ucfirst($name) ,'hornbill') ?></label>
            <input 
                type="text" 
                name="<?php echo $this->get_field_name($name); ?>" 
                id="<?php echo $this->get_field_id($name); ?>" 
                value="<?php echo isset($instance[$name]) ? $instance[$name] : ''; ?>" 
                class="widefat">
        </p>        

        <p>
            <?php $name = 'bio';  ?>
            <label for="<?php echo $this->get_field_id($name); ?>"><?php echo esc_html__( ucfirst($name) ,'hornbill') ?></label>
            <textarea 
                type="text" 
                name="<?php echo $this->get_field_name($name); ?>" 
                id="<?php echo $this->get_field_id($name); ?>" 
                class="widefat"><?php echo isset($instance[$name]) ? $instance[$name] : ''; ?>
            </textarea>
        </p>
        

        <p>

            <?php $name = 'photo';  ?>
            <?php if(isset($instance[$name])): ?>
                <img style="max-width: 100%;" src="<?php echo isset($instance[$name]) ? $instance[$name] : ''; ?>" />
            <?php endif; ?>
            <label style="margin-top: 5px;display: block;" for="<?php echo $this->get_field_id($name); ?>"><?php echo esc_html__( ucfirst($name) ,'hornbill') ?></label>
            <input 
                type="text" 
                name="<?php echo $this->get_field_name($name); ?>" 
                id="<?php echo $this->get_field_id($name); ?>" 
                value="<?php echo isset($instance[$name]) ? $instance[$name] : ''; ?>" 
                class="widefat">
            <button class="upload_image_button button" style="margin-top: 5px;">
                <?php echo strlen($instance[$name]) ? "Replace Image" : "Upload Image"; ?>
            </button>

            <?php if(strlen($instance[$name])): ?>
            <button class="button clear_image">clear</button>
            <?php endif; ?>
        </p>

	<?php }
}

add_action( 'widgets_init', function(){
	register_widget( 'Hornbill_Author_info_widget' );
});
