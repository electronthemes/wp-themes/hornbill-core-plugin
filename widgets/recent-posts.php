<?php
class Hornbill_RecentPost_Widget extends WP_Widget {

    public function __construct() {
        $widget_ops = array( 
            'classname' => 'Hornbill_RecentPost_Widget',
        );
        parent::__construct( 'Hornbill_RecentPost_Widget', 'Hornbill: Recent Posts', $widget_ops );
    }

    /**
     * Outputs the content of the widget
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) { 
        echo $args['before_widget'] . $args['before_title'] . $instance['title'] . $args['after_title'];
    ?>
    <?php 
        $query = new WP_Query( array(
            'post_type' => array( 'post' ),
            'posts_per_page' => $instance['post_count'],
        ) );
        while ( $query->have_posts() ) :
        $query->the_post();
    ?>
    <div <?php post_class('media'); ?>>
        <?php if(has_post_thumbnail()): ?>
        <div class="media-left">
            <a href="<?php echo esc_url( get_the_permalink() ); ?>">
                <?php the_post_thumbnail('recent_post_widget'); ?>
            </a>
        </div>
        <?php endif; ?>
        <div class="media-body">
            <h4 class="media-heading"><a href="<?php echo esc_url( get_the_permalink() ); ?>"><?php the_title(); ?></a></h4>
            <p class="date"><?php the_time('F d, Y'); ?></p>
        </div>
    </div>
    <?php 
        endwhile;
        wp_reset_postdata();
    ?>
    <?php echo $args['after_widget']; }
    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) { ?>
        <p>
            <?php $name = 'title';  ?>
            <label for="<?php echo $this->get_field_id($name); ?>"><?php echo esc_html__( ucfirst($name) ,'hornbill') ?></label>
            <input 
                type="text" 
                name="<?php echo $this->get_field_name($name); ?>" 
                id="<?php echo $this->get_field_id($name); ?>" 
                value="<?php echo isset($instance[$name]) ? $instance[$name] : ''; ?>" 
                class="widefat">
        </p>
        <p>
            <?php 
            $title = 'Number of posts to show';
            $name = 'post_count';  ?>
            <label for="<?php echo $this->get_field_id($name); ?>"><?php echo esc_html__( ucfirst($title) ,'hornbill') ?></label>
            <input 
                type="text" 
                name="<?php echo $this->get_field_name($name); ?>" 
                id="<?php echo $this->get_field_id($name); ?>" 
                value="<?php echo isset($instance[$name]) ? $instance[$name] : ''; ?>" 
                class="tiny-text">
        </p>
    <?php }
}

add_action( 'widgets_init', function(){
    register_widget( 'Hornbill_RecentPost_Widget' );
});
