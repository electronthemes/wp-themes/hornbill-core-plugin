<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$bi_icons = [
    "bi-accordion-horizontal" => "bi-accordion-horizontal",
    "bi-accordion-vertical" => "bi-accordion-vertical",
    "bi-alarm-clock" => "bi-alarm-clock",
    "bi-alien" => "bi-alien",
    "bi-alien-gun" => "bi-alien-gun",
    "bi-anchor" => "bi-anchor",
    "bi-android" => "bi-android",
    "bi-apple" => "bi-apple",
    "bi-arrow-left-rounded" => "bi-arrow-left-rounded",
    "bi-arrow-left-square" => "bi-arrow-left-square",
    "bi-arrow-right-rounded" => "bi-arrow-right-rounded",
    "bi-arrow-right-square" => "bi-arrow-right-square",
    "bi-article" => "bi-article",
    "bi-badge1" => "bi-badge1",
    "bi-badge2" => "bi-badge2",
    "bi-badge3" => "bi-badge3",
    "bi-bamboo" => "bi-bamboo",
    "bi-battery" => "bi-battery",
    "bi-beach-seat" => "bi-beach-seat",
    "bi-bicycle" => "bi-bicycle",
    "bi-blog" => "bi-blog",
    "bi-blue-tooth" => "bi-blue-tooth",
    "bi-board" => "bi-board",
    "bi-body" => "bi-body",
    "bi-bond" => "bi-bond",
    "bi-book" => "bi-book",
    "bi-bowl" => "bi-bowl",
    "bi-brick-wall" => "bi-brick-wall",
    "bi-brush" => "bi-brush",
    "bi-brush-paint" => "bi-brush-paint",
    "bi-brush-roll" => "bi-brush-roll",
    "bi-bug" => "bi-bug",
    "bi-bulb" => "bi-bulb",
    "bi-calculation" => "bi-calculation",
    "bi-calendar" => "bi-calendar",
    "bi-camera" => "bi-camera",
    "bi-candle" => "bi-candle",
    "bi-candles" => "bi-candles",
    "bi-car" => "bi-car",
    "bi-carousal" => "bi-carousal",
    "bi-caution" => "bi-caution",
    "bi-chair1" => "bi-chair1",
    "bi-chair2" => "bi-chair2",
    "bi-chat-bubble" => "bi-chat-bubble",
    "bi-chat-bubble-single" => "bi-chat-bubble-single",
    "bi-cheese" => "bi-cheese",
    "bi-clip" => "bi-clip",
    "bi-clip-board" => "bi-clip-board",
    "bi-cloud" => "bi-cloud",
    "bi-cloud-down" => "bi-cloud-down",
    "bi-cloud-up" => "bi-cloud-up",
    "bi-code" => "bi-code",
    "bi-code-browser" => "bi-code-browser",
    "bi-code-clean" => "bi-code-clean",
    "bi-color-plate" => "bi-color-plate",
    "bi-compass" => "bi-compass",
    "bi-compass-math" => "bi-compass-math",
    "bi-crop" => "bi-crop",
    "bi-cross" => "bi-cross",
    "bi-cross-game" => "bi-cross-game",
    "bi-cross-gap" => "bi-cross-gap",
    "bi-crown" => "bi-crown",
    "bi-cube" => "bi-cube",
    "bi-cup" => "bi-cup",
    "bi-cup-coffee" => "bi-cup-coffee",
    "bi-delivery-van" => "bi-delivery-van",
    "bi-direction-both" => "bi-direction-both",
    "bi-direction-right" => "bi-direction-right",
    "bi-disc" => "bi-disc",
    "bi-dislike" => "bi-dislike",
    "bi-door-path" => "bi-door-path",
    "bi-drag" => "bi-drag",
    "bi-drag-inside" => "bi-drag-inside",
    "bi-drag-outside" => "bi-drag-outside",
    "bi-drawer" => "bi-drawer",
    "bi-dribbble" => "bi-dribbble",
    "bi-dropper" => "bi-dropper",
    "bi-ellipsis-horizontal" => "bi-ellipsis-horizontal",
    "bi-ellipsis-vertical" => "bi-ellipsis-vertical",
    "bi-emo-normal" => "bi-emo-normal",
    "bi-emo-sad" => "bi-emo-sad",
    "bi-emo-smile" => "bi-emo-smile",
    "bi-envelop" => "bi-envelop",
    "bi-facebook" => "bi-facebook",
    "bi-female" => "bi-female",
    "bi-file" => "bi-file",
    "bi-file-cabinet" => "bi-file-cabinet",
    "bi-files" => "bi-files",
    "bi-film" => "bi-film",
    "bi-film-roll" => "bi-film-roll",
    "bi-finger-index" => "bi-finger-index",
    "bi-finger-print" => "bi-finger-print",
    "bi-flag" => "bi-flag",
    "bi-folder" => "bi-folder",
    "bi-footer" => "bi-footer",
    "bi-form" => "bi-form",
    "bi-forward" => "bi-forward",
    "bi-gender" => "bi-gender",
    "bi-gender-female" => "bi-gender-female",
    "bi-gender-male" => "bi-gender-male",
    "bi-gender-sign" => "bi-gender-sign",
    "bi-ghost" => "bi-ghost",
    "bi-gift-box" => "bi-gift-box",
    "bi-globe1" => "bi-globe1",
    "bi-globe2" => "bi-globe2",
    "bi-globe3" => "bi-globe3",
    "bi-globe4" => "bi-globe4",
    "bi-google" => "bi-google",
    "bi-graduation-cap" => "bi-graduation-cap",
    "bi-graph-bar" => "bi-graph-bar",
    "bi-graph-pie" => "bi-graph-pie",
    "bi-grid" => "bi-grid",
    "bi-grid-even" => "bi-grid-even",
    "bi-grid-masonry" => "bi-grid-masonry",
    "bi-grid-twist" => "bi-grid-twist",
    "bi-group" => "bi-group",
    "bi-hand" => "bi-hand",
    "bi-hand-mike" => "bi-hand-mike",
    "bi-hand-watch" => "bi-hand-watch",
    "bi-header" => "bi-header",
    "bi-headphone" => "bi-headphone",
    "bi-headset" => "bi-headset",
    "bi-heart-beat" => "bi-heart-beat",
    "bi-highlighter" => "bi-highlighter",
    "bi-home" => "bi-home",
    "bi-hotdog" => "bi-hotdog",
    "bi-ice-cream" => "bi-ice-cream",
    "bi-image" => "bi-image",
    "bi-jar" => "bi-jar",
    "bi-laptop" => "bi-laptop",
    "bi-layer" => "bi-layer",
    "bi-lens" => "bi-lens",
    "bi-like" => "bi-like",
    "bi-link" => "bi-link",
    "bi-linkedin" => "bi-linkedin",
    "bi-linux" => "bi-linux",
    "bi-list" => "bi-list",
    "bi-location-pointer" => "bi-location-pointer",
    "bi-lock" => "bi-lock",
    "bi-love" => "bi-love",
    "bi-madel" => "bi-madel",
    "bi-magnet" => "bi-magnet",
    "bi-male" => "bi-male",
    "bi-map-pointer" => "bi-map-pointer",
    "bi-measurement" => "bi-measurement",
    "bi-microphone1" => "bi-microphone1",
    "bi-microphone2" => "bi-microphone2",
    "bi-mobile" => "bi-mobile",
    "bi-money" => "bi-money",
    "bi-money-bag" => "bi-money-bag",
    "bi-monitor" => "bi-monitor",
    "bi-mouse" => "bi-mouse",
    "bi-muscle" => "bi-muscle",
    "bi-nuclear-circle" => "bi-nuclear-circle",
    "bi-office-bag" => "bi-office-bag",
    "bi-pacman" => "bi-pacman",
    "bi-paper" => "bi-paper",
    "bi-paper-fold" => "bi-paper-fold",
    "bi-paper-plane" => "bi-paper-plane",
    "bi-pen" => "bi-pen",
    "bi-pencil" => "bi-pencil",
    "bi-pendrive" => "bi-pendrive",
    "bi-phone" => "bi-phone",
    "bi-pillar" => "bi-pillar",
    "bi-pin" => "bi-pin",
    "bi-pin-man" => "bi-pin-man",
    "bi-pin-man-range" => "bi-pin-man-range",
    "bi-plane" => "bi-plane",
    "bi-play-button" => "bi-play-button",
    "bi-playing-card" => "bi-playing-card",
    "bi-play-store" => "bi-play-store",
    "bi-plus" => "bi-plus",
    "bi-plus-gap" => "bi-plus-gap",
    "bi-power" => "bi-power",
    "bi-printer" => "bi-printer",
    "bi-quote" => "bi-quote",
    "bi-radar" => "bi-radar",
    "bi-radiation" => "bi-radiation",
    "bi-recycle" => "bi-recycle",
    "bi-refresh-time" => "bi-refresh-time",
    "bi-reply" => "bi-reply",
    "bi-responsive-device" => "bi-responsive-device",
    "bi-rocket1" => "bi-rocket1",
    "bi-rocket2" => "bi-rocket2",
    "bi-rss" => "bi-rss",
    "bi-safety-cap" => "bi-safety-cap",
    "bi-safety-kit" => "bi-safety-kit",
    "bi-sand-watch" => "bi-sand-watch",
    "bi-scale" => "bi-scale",
    "bi-scanner" => "bi-scanner",
    "bi-scissor" => "bi-scissor",
    "bi-screen" => "bi-screen",
    "bi-search" => "bi-search",
    "bi-server" => "bi-server",
    "bi-share" => "bi-share",
    "bi-shield" => "bi-shield",
    "bi-shopping-bag1" => "bi-shopping-bag1",
    "bi-shopping-bag2" => "bi-shopping-bag2",
    "bi-shopping-bag3" => "bi-shopping-bag3",
    "bi-shopping-bag4" => "bi-shopping-bag4",
    "bi-shopping-cart-emply" => "bi-shopping-cart-emply",
    "bi-shopping-cart-full" => "bi-shopping-cart-full",
    "bi-shutter1" => "bi-shutter1",
    "bi-sign-in" => "bi-sign-in",
    "bi-sign-out" => "bi-sign-out",
    "bi-sitemap1" => "bi-sitemap1",
    "bi-sitemap2" => "bi-sitemap2",
    "bi-slider" => "bi-slider",
    "bi-slider-doc" => "bi-slider-doc",
    "bi-slider-filter" => "bi-slider-filter",
    "bi-slider-image" => "bi-slider-image",
    "bi-slider-range" => "bi-slider-range",
    "bi-slider-video" => "bi-slider-video",
    "bi-smart-watch" => "bi-smart-watch",
    "bi-spa-face" => "bi-spa-face",
    "bi-spark" => "bi-spark",
    "bi-spa-stone" => "bi-spa-stone",
    "bi-spa-stone-flower" => "bi-spa-stone-flower",
    "bi-speaker-off" => "bi-speaker-off",
    "bi-speaker-on" => "bi-speaker-on",
    "bi-steps" => "bi-steps",
    "bi-stop-watch" => "bi-stop-watch",
    "bi-support" => "bi-support",
    "bi-tab" => "bi-tab",
    "bi-table-lamp" => "bi-table-lamp",
    "bi-tablet" => "bi-tablet",
    "bi-tag" => "bi-tag",
    "bi-target" => "bi-target",
    "bi-target-arrow" => "bi-target-arrow",
    "bi-terminal" => "bi-terminal",
    "bi-tick" => "bi-tick",
    "bi-tie" => "bi-tie",
    "bi-tie-knot" => "bi-tie-knot",
    "bi-tools" => "bi-tools",
    "bi-tree" => "bi-tree",
    "bi-twitter" => "bi-twitter",
    "bi-twitter-bird" => "bi-twitter-bird",
    "bi-ufo" => "bi-ufo",
    "bi-umbralla" => "bi-umbralla",
    "bi-unlock" => "bi-unlock",
    "bi-up-down" => "bi-up-down",
    "bi-user-ID" => "bi-user-ID",
    "bi-video-cam" => "bi-video-cam",
    "bi-weather-cloud" => "bi-weather-cloud",
    "bi-webcam1" => "bi-webcam1",
    "bi-webcam2" => "bi-webcam2",
    "bi-windows" => "bi-windows",
    "bi-wine-glass" => "bi-wine-glass",
    "bi-worker-cap" => "bi-worker-cap",
    "bi-youtube" => "bi-youtube"
];

define('BI_ICONS' , $bi_icons);


/**
 * Hornbill_Elementor_Main Class
 *
 * The init class that runs the Hello World plugin.
 * Intended To make sure that the plugin's minimum requirements are met.
 *
 * You should only modify the constants to match your plugin's needs.
 *
 * Any custom code should go inside Plugin Class in the plugin.php file.
 * @since 1.2.0
 */
final class Hornbill_Elementor_Main {

	/**
	 * Plugin Version
	 *
	 * @since 1.2.0
	 * @var string The plugin version.
	 */
	const VERSION = '1.2.0';

	/**
	 * Minimum Elementor Version
	 *
	 * @since 1.2.0
	 * @var string Minimum Elementor version required to run the plugin.
	 */
	const MINIMUM_ELEMENTOR_VERSION = '2.0.0';

	/**
	 * Minimum PHP Version
	 *
	 * @since 1.2.0
	 * @var string Minimum PHP version required to run the plugin.
	 */
	const MINIMUM_PHP_VERSION = '7.0';

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function __construct() {

		// Load translation
		add_action( 'init', array( $this, 'i18n' ) );

		// Init Plugin
		add_action( 'plugins_loaded', array( $this, 'init' ) );
	}

	/**
	 * Load Textdomain
	 *
	 * Load plugin localization files.
	 * Fired by `init` action hook.
	 *
	 * @since 1.2.0
	 * @access public
	 */
	public function i18n() {
		load_plugin_textdomain( 'hormbill-core' );
	}

	/**
	 * Initialize the plugin
	 *
	 * Validates that Elementor is already loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed include the plugin class.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.2.0
	 * @access public
	 */
	public function init() {

		// Check if Elementor installed and activated
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', array( $this, 'admin_notice_missing_main_plugin' ) );
			return;
		}

		// Check for required Elementor version
		if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
			add_action( 'admin_notices', array( $this, 'admin_notice_minimum_elementor_version' ) );
			return;
		}

		// Check for required PHP version
		if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
			add_action( 'admin_notices', array( $this, 'admin_notice_minimum_php_version' ) );
			return;
		}

		// Once we get here, We have passed all validation checks so we can safely include our plugin
		require_once( 'plugin.php' );
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Elementor installed or activated.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'hornbill-core' ),
			'<strong>' . esc_html__( 'Hoenbill Core', 'hornbill-core' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'hornbill-core' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required Elementor version.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function admin_notice_minimum_elementor_version() {
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'hormbill-core' ),
			'<strong>' . esc_html__( 'Hoenbill Core', 'hormbill-core' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'hormbill-core' ) . '</strong>',
			self::MINIMUM_ELEMENTOR_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}

		$message = sprintf(
			/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'hormbill-core' ),
			'<strong>' . esc_html__( 'Hoenbill Core', 'hormbill-core' ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', 'hormbill-core' ) . '</strong>',
			self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );
	}
}

// Instantiate Elementor_Hello_World.
new Hornbill_Elementor_Main();




// Register Hornbill Category
function add_elementor_widget_categories( $elements_manager ) {

    $elements_manager->add_category(
        'hornbill',
        [
            'title' => __( 'Hornbill', 'hornbill-core' ),
            'icon' => 'fa fa-plug',
        ]
    );

}
add_action( 'elementor/elements/categories_registered', 'add_elementor_widget_categories' );
