<?php
namespace HornbillElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class Hornbill_Recent_Posts extends Widget_Base {

    /**
     * Retrieve the widget name.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget name.
     */
    public function get_name() {
        return 'hornbill-recent-post';
    }

    /**
     * Retrieve the widget title.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget title.
     */
    public function get_title() {
        return __( 'Posts', 'hornbill-core' );
    }

    /**
     * Retrieve the widget icon.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget icon.
     */
    public function get_icon() {
        return 'eicon-posts-grid';
    }

    /**
     * Retrieve the list of categories the widget belongs to.
     *
     * Used to determine where to display the widget in the editor.
     *
     * Note that currently Elementor supports only one category.
     * When multiple categories passed, Elementor uses the first one.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return array Widget categories.
     */
    public function get_categories() {
        return [ 'hornbill' ];
    }

    /**
     * Register the widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _register_controls() {

        // Contents
        $this->start_controls_section(
            'section_content',
            [
                'label' => __( 'Posts Option', 'hornbill-core' ),
            ]
        );

        $this->add_control( 'post_option' , [
            'label' => __( 'Post Options', 'hornbill-core' ),
            'type' => Controls_Manager::CHOOSE,
            'options' => [
                'recent' => [
                    'title' => __( 'Recent Posts', 'hornbill-core' ),
                    'icon' => 'fa fa-th',
                ],
                'category' => [
                    'title' => __( 'Category', 'hornbill-core' ),
                    'icon' => 'fa fa-clone',
                ],
                'author' => [
                    'title' => __( 'Author', 'hornbill-core' ),
                    'icon' => 'fa fa-user',
                ],
            ],
            'default' => 'recent',
            'toggle' => true,
        ] );



        $this->add_control('post_counter', [
             'label' => __('Posts show', 'hornbill-core'),
            'type' => Controls_Manager::SLIDER,
            'description' => __('Drag number to show the posts in the page', 'hornbill-core'),
            'label_block' => true,
            'range' => [
                'px' => [
                    'min' => 1,
                    'max' => 15,
                    'step' => 1
                ]
            ],
            'default' => [
                'unit' => 'px',
                'size' => 6
            ]
        ]);

        $this->add_control('post-card-theme', [
             'label' => __('Card Theme', 'hornbill-core'),
            'type' => Controls_Manager::SELECT,
            'description' => __('Select posts card themes', 'hornbill-core'),
            'options' => [
                    'smart' => __('Smart', 'hornbill-core'),
                    'classic' => __('Classic', 'hornbill-core')
            ],
            'default' => 'classic'
        ]);

        $this->add_control('post-card-column', [
            'label' => __('Card Column Style', 'hornbill-core'),
            'type' => Controls_Manager::SELECT,
            'description' => __('Select posts card column style', 'hornbill-core'),
            'options' => [
                'masonry' => __('Masonry', 'hornbill-core'),
                'row' => __('Row', 'hornbill-core')
            ],
            'default' => 'masonry'
        ]);

        $this->end_controls_section();

    }

    /**
     * Render the widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function render() {
        $settings = $this->get_settings_for_display();

        $post = new \WP_Query(array(
                'posts_per_page' => $settings['post_counter']['size'],
                'post_type' => 'post'
        ))
    ?>
        <?php
            // TODO: Post card clasic style on editor
            // TODO: Real Time Load using Backbone
        ?>
        <?php if($post->have_posts()): ?>
        <?php if($settings['post-card-theme'] == 'classic'): ?>
        <div class="col-sm-12">

            <div class="card-blogs column-three">
                <?php while($post->have_posts()): $post->the_post() ?>
                <div class="card single-blog m-bot-30 style-two blog-card-classic text-white">
                    <?php if(has_post_thumbnail()): ?>
                    <a href="<?php echo esc_url(get_the_permalink()); ?>">
                        <?php the_post_thumbnail( 'card_lard', array('class' => 'card-img-top single-blog-img') ) ?>
                    </a>
                    <?php endif; ?>
                    <div class="card-body">
                        <div class="post-card-cats">
                            <?php
                            the_category( ' ' );
                            ?>
                        </div>
                        <h3 class="card-title"><a href="<?php echo esc_url(get_the_permalink()); ?>"><?php the_title(); ?></a></h3>
                        <p><?php echo wp_trim_words(get_the_content(), '30', ''); ?></p>
                        <h6 class="info"><?php echo get_the_date(); ?></h6>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <!-- end card columns -->
        </div>
        <!-- end col -->
        <?php else: ?>
            <div class="row">
            <?php while($post->have_posts()): $post->the_post() ?>
                <div class="col-md-4">
                    <div class="card single-blog m-bot-30 fadeIn">
                        <?php if(has_post_thumbnail()): ?>
                            <a href="<?php echo esc_url(get_the_permalink()); ?>">
                                <?php the_post_thumbnail( 'card_lard', array('class' => 'card-img-top single-blog-img') ) ?>
                            </a>
                        <?php endif; ?>
                        <div class="card-body">
                            <a href="#" class=" btn btn-category bg-sharp text-white hover-glass">travel</a>
                            <h4 class="card-title"><a href="<?php echo esc_url(get_the_permalink()); ?>"><?php the_title(); ?></a></h4>
                            <p><?php echo wp_trim_words(get_the_content(), '20', ''); ?></p>
                        </div>
                        <div class="card-footer clearfix">
                            <div class="author">
                                <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
                                    <span class="author-img">
                                        <?php echo
                                        get_avatar(
                                            get_the_author_meta( 'ID' ) , // id
                                            100 , '' , // size
                                            get_the_author_meta( 'display_name' ) , // alt
                                            array('class' => 'img-fluid') // args
                                        )
                                        ?>
                                    </span>
                                    <?php echo get_the_author(); ?>
                                </a>
                            </div>
                            <div class="comments">
                                <a href="<?php echo esc_url(get_the_permalink()); ?>">
                                    <?php comments_number( '0', '1', '%' ); ?>
                                    <span class="fa fa-comments-o"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of single blog -->
            <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <?php else: ?>
        <h3 class="text-center">No posts found</h3>
        <?php endif; ?>
    <?php }

    /**
     * Render the widget output in the editor.
     *
     * Written as a Backbone JavaScript template and used to generate the live preview.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _content_template() { ?>
    <?php }
}