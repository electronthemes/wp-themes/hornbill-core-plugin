<?php
namespace HornbillElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class Big_Button extends Widget_Base {

    /**
     * Retrieve the widget name.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget name.
     */
    public function get_name() {
        return 'big-button';
    }

    /**
     * Retrieve the widget title.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget title.
     */
    public function get_title() {
        return __( 'Big Button', 'hornbill-core' );
    }

    /**
     * Retrieve the widget icon.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget icon.
     */
    public function get_icon() {
        return 'eicon-posts-ticker';
    }

    /**
     * Retrieve the list of categories the widget belongs to.
     *
     * Used to determine where to display the widget in the editor.
     *
     * Note that currently Elementor supports only one category.
     * When multiple categories passed, Elementor uses the first one.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return array Widget categories.
     */
    public function get_categories() {
        return [ 'hornbill' ];
    }

    /**
     * Retrieve the list of scripts the widget depended on.
     *
     * Used to set scripts dependencies required to run the widget.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return array Widget scripts dependencies.
     */
    public function get_script_depends() {
        return [ 'elementor-hello-world' ];
    }

    /**
     * Register the widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _register_controls() {
        $this->start_controls_section(
            'section_content',
            [
                'label' => __( 'Widget Contents', 'hornbill-core' ),
            ]
        );

        $this->add_control(
            'smalltext',
            [
                'label' => __( 'Small Text', 'hornbill-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( 'download for', 'hornbill-core' )
            ]
        );
        $this->add_control(
            'url',
            [
                'label' => __( 'Url', 'hornbill-core' ),
                'type' => Controls_Manager::URL,
            ]
        );
        $this->add_control(
            'largetext',
            [
                'label' => __( 'Large Text', 'hornbill-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'iphone'
            ]
        );
        $this->add_control(
            'icon',
            [
                'label' => __( 'Icon', 'hornbill-core' ),
                'type' => Controls_Manager::ICON,
                'default' => 'fa fa-apple'
            ]
        );
        $this->add_responsive_control(
            'icon_size',
            [
                'label' => __( 'Icon Size', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'default' => [
                    'unit' => 'px',
                    'size' => 43,
                ],
                'range' => [
                    'px' => [
                        'min' => 5,
                        'max' => 500,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .icon' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_control(
            'theme',
            [
                'label' => __( 'Button Gradient', 'hornbill-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'acid' => __('acid' , 'hornbill-core'),
                    'beach' => __('beach' , 'hornbill-core'),
                    'damn' => __('damn' , 'hornbill-core'),
                    'darker' => __('darker' , 'hornbill-core'),
                    'grass' => __('grass' , 'hornbill-core'),
                    'gray' => __('gray' , 'hornbill-core'),
                    'ripe' => __('ripe' , 'hornbill-core'),
                    'sharp' => __('sharp' , 'hornbill-core'),
                    'sky' => __('sky' , 'hornbill-core'),
                    'splash' => __('splash' , 'hornbill-core'),
                    'sunshine' => __('sunshine' , 'hornbill-core'),
                    'victoria' => __('victoria' , 'hornbill-core'),
                    'wine' => __('wine' , 'hornbill-core')
                ],
                'default' => 'damn'
            ]
        );

        $this->end_controls_section();
    }

    /**
     * Render the widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function render() {
        $settings = $this->get_settings_for_display();
        $target = $settings['url']['is_external'] ? ' target="_blank"' : '';
        $nofollow = $settings['url']['nofollow'] ? ' rel="nofollow"' : '';
    ?>

        <a href="<?php echo $settings['url']['url']; ?>" class="btn btn-circle hornbill-big-btn bg-<?php echo $settings['theme'] ?> hover-glass" <?php echo $target . $nofollow; ?>>
            <i class="<?php echo $settings['icon'] ?> icon"></i>
            <span class="normal-txt"><?php echo $settings['smalltext'] ?></span>
            <span class="big-txt"><?php echo $settings['largetext'] ?></span>
        </a>

    <?php }

    /**
     * Render the widget output in the editor.
     *
     * Written as a Backbone JavaScript template and used to generate the live preview.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _content_template() { ?>
        <#
            var target = settings.url.is_external ? ' target="_blank"' : '';
            var nofollow = settings.url.nofollow ? ' rel="nofollow"' : '';
        #>

        <a href="{{ settings.url.url }}" class="btn btn-circle hornbill-big-btn bg-{{ settings.theme }} hover-glass" {{ target }}{{ nofollow }}>
            <i class="{{ settings.icon }} icon"></i>
            <span class="normal-txt">{{ settings.smalltext }}</span>
            <span class="big-txt">{{ settings.largetext }}</span>
        </a>


    <?php }
}
