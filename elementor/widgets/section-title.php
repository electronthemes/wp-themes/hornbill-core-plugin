<?php
namespace HornbillElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class Hornbill_Section_title extends Widget_Base {

    /**
     * Retrieve the widget name.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget name.
     */
    public function get_name() {
        return 'hornbill-section-title';
    }

    /**
     * Retrieve the widget title.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget title.
     */
    public function get_title() {
        return __( 'Section Title', 'hornbill-core' );
    }

    /**
     * Retrieve the widget icon.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget icon.
     */
    public function get_icon() {
        return 'eicon-archive-title';
    }

    /**
     * Retrieve the list of categories the widget belongs to.
     *
     * Used to determine where to display the widget in the editor.
     *
     * Note that currently Elementor supports only one category.
     * When multiple categories passed, Elementor uses the first one.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return array Widget categories.
     */
    public function get_categories() {
        return [ 'hornbill' ];
    }

    /**
     * Register the widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _register_controls() {

        // Contents
        $this->start_controls_section(
            'section_content',
            [
                'label' => __( 'Widget Contents', 'hornbill-core' ),
            ]
        );

        $this->add_control( 'title' , [
            'label' => __('Title' , 'hornbill-core'),
            'type' => Controls_Manager::TEXT,
            'label_block' => true,
            'default' => __('Hornbill Section Title' , 'hornbill-core')
        ] );

        $this->add_control( 'desc' , [
            'label' => __('Description' , 'hornbill-core'),
            'type' => Controls_Manager::TEXTAREA,
            'default' => __('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis rhoncus ante, eget molestie dolor. Duis tellus magna, malesuada vitae velit in, ultrices fringilla metus. Nulla facilisi.' , 'hornbill-core')
        ] );
        $this->end_controls_section();


        // ---------------------------------------
        // STyle Tab
        // ---------------------------------------

        // Title
        
        $this->start_controls_section(
            'style_title',
            [
                'label' => __( 'Title', 'hornbill-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
                ]
            );
            
            $this->add_responsive_control(
            'title_font_size',
            [
                'label' => __( 'Font Size', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px','em'],
                'default' => [
                    'unit' => 'px',
                    'size' => 45,
                ],
                'range' => [
                    'px' => [
                        'min' => 5,
                        'max' => 500,
                    ],
                    'em' => [
                        'min' => .1,
                        'max' => 10
                    ]
                ],
                'selectors' => [
                    '{{WRAPPER}} .title' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control( 'title_color' , [
            'label' => __('Color' , 'hornbill-core'),
            'type' => Controls_Manager::COLOR,
            'default' => '#7f67f3',
            'selectors' => [
                '{{WRAPPER}} .title' => 'color: {{VALUE}};',
            ],
        ] );


        $this->end_controls_section();
            
            
        // Desc
        $this->start_controls_section(
            'style_desc',
            [
                'label' => __( 'Description', 'hornbill-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'desc_font_size',
            [
                'label' => __( 'Font Size', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px','em'],
                'default' => [
                    'unit' => 'px',
                    'size' => 16,
                ],
                'range' => [
                    'px' => [
                        'min' => 5,
                        'max' => 500,
                    ],
                    'em' => [
                        'min' => .1,
                        'max' => 10
                    ]
                ],
                'selectors' => [
                    '{{WRAPPER}} .desc' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );


        $this->add_responsive_control(
            'desc_line_height',
            [
                'label' => __( 'Line height', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px','em'],
                'default' => [
                    'unit' => 'px',
                    'size' => 26,
                ],
                'range' => [
                    'px' => [
                        'min' => 5,
                        'max' => 500,
                    ],
                    'em' => [
                        'min' => .1,
                        'max' => 10
                    ]
                ],
                'selectors' => [
                    '{{WRAPPER}} .desc' => 'line-height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );


        $this->add_responsive_control( 'desc_color' , [
            'label' => __('Color' , 'hornbill-core'),
            'type' => Controls_Manager::COLOR,
            'default' => '#787878',
            'selectors' => [
                '{{WRAPPER}} .desc' => 'color: {{VALUE}};',
            ],
        ] );
        

        
        $this->end_controls_section();


        $this->start_controls_section(
            'style_sep',
            [
                'label' => __( 'Separator', 'hornbill-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control( 'show_sep' , [
            'label' => __('Show Seperator' , 'hornbill-core'),
            'type' => Controls_Manager::SWITCHER,
            'label_on' => __('Show' , 'hornbill-core'),
            'label_off' => __('Hide' , 'hornbill-core'),
            'default' => 'show',
            'return_value' => 'show',
        ] );


        $this->add_control( 'sep_icon' , [
            'label' => __('Show Seperator' , 'hornbill-core'),
            'description' => __('See all icons <a href=\'http://bicon.lab.themebucket.net/\' target="_blank">here</a>' , 'hornbill-core'),
            'type' => Controls_Manager::SELECT2,
            'options' => BI_ICONS,
            'default' => 'bi-shutter1'
        ] );

        $this->add_responsive_control(
            'sep_width',
            [
                'label' => __( 'Line Width', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'default' => [
                    'unit' => 'px',
                    'size' => 60,
                ],
                'range' => [
                    'px' => [
                        'min' => 30,
                        'max' => 200,
                    ]
                ],
                'selectors' => [
                    '{{WRAPPER}} .line' => 'width: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'sep_color',
            [
                'label' => __( 'Color', 'hornbill-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#7f67f3',
                'selectors' => [
                    '{{WRAPPER}} .bi' => 'color: {{VALUE}};',
                    '{{WRAPPER}} .line ' => 'background-color: {{VALUE}};'
                ],
            ]
        );

        $this->add_responsive_control(
            'sep_spacing',
            [
                'label' => __( 'Margin', 'hornbill-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', 'em'],
                'allowed_dimensions' => ['top', 'bottom'],
                'default' => [
                    'top' => '15px',
                    'right' => '0',
                    'bottom' => '20px',
                    'left' => '0',
                    'unit' => 'px',
                    'isLinked' => false
                ],
                'selectors' => [
                    '{{WRAPPER}} .heading-lines' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $this->end_controls_section();

    }

    /**
     * Render the widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function render() {
        $settings = $this->get_settings_for_display();
    ?>
        <div class="section-heading m-bot-60 m-sm-bot-0 fadeIn">
            <div class="col-md-8 ml-md-auto mr-md-auto">
                <h2 class="title"><?php echo $settings['title']; ?></h2>
                <?php if('show' == $settings['show_sep']): ?>
                <div class="heading-lines d-flex justify-content-center clearfix">
                    <div class="horn-sep">
                        <span class="line"></span>
                        <span class="bi <?php echo $settings['sep_icon']; ?>"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <?php endif; ?>
                <p class="desc"><?php echo $settings['desc']; ?></p>
            </div>
        </div>
    <?php }

    /**
     * Render the widget output in the editor.
     *
     * Written as a Backbone JavaScript template and used to generate the live preview.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _content_template() { ?>
        <div class="section-heading m-bot-60 m-sm-bot-0 fadeIn">
            <div class="col-md-8 ml-md-auto mr-md-auto">
                <h2 class="title">{{ settings.title }}</h2>
                <# if('show' == settings.show_sep) { #>
                <div class="heading-lines d-flex justify-content-center clearfix">
                    <div class="horn-sep">
                        <span class="line"></span>
                        <span class="bi {{ settings.sep_icon }}"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <# } #>
                <p>{{ settings.desc }}</p>
            </div>
        </div>
    <?php }
}