<?php
namespace HornbillElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class Service_Icon extends Widget_Base {

    /**
     * Retrieve the widget name.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget name.
     */
    public function get_name() {
        return 'service-icon';
    }

    /**
     * Retrieve the widget title.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget title.
     */
    public function get_title() {
        return __( 'Service Icon', 'hornbill-core' );
    }

    /**
     * Retrieve the widget icon.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget icon.
     */
    public function get_icon() {
        return 'eicon-posts-ticker';
    }

    /**
     * Retrieve the list of categories the widget belongs to.
     *
     * Used to determine where to display the widget in the editor.
     *
     * Note that currently Elementor supports only one category.
     * When multiple categories passed, Elementor uses the first one.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return array Widget categories.
     */
    public function get_categories() {
        return [ 'hornbill' ];
    }

    /**
     * Register the widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _register_controls() {
        $this->start_controls_section(
            'section_content',
            [
                'label' => __( 'Widget Contents', 'hornbill-core' ),
            ]
        );

        $this->add_control(
            'image',
            [
                'label' => __( 'Icon Image', 'hornbill-core' ),
                'type' => Controls_Manager::MEDIA
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'hornbill-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( 'Great Interface', 'hornbill-core' )
            ]
        );
        $this->add_control(
            'desc',
            [
                'label' => __( 'Description', 'hornbill-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => __( 'Sed vehculia semiper sapen quis, gravida massa cursus luctus. Cras at feugiat urna.', 'hornbill-core' )
            ]
        );

        $this->add_control(
            'type',
            [
                'label' => __( 'Type', 'hornbill-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'style-one' => __('Style one' , 'hornbill-core'),
                    'style-two' => __('Style two' , 'hornbill-core')
                ],
                'default' => 'style-one'
            ]
        );
        $this->end_controls_section();

        // ----------- Title Style -----------
        $this->start_controls_section(
            'style_title_tab',
            [
                'label' => __( 'Title', 'hornbill-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_responsive_control(
            'title_font_size',
            [
                'label' => __( 'Font Size', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px','em'],
                'default' => [
                    'unit' => 'px',
                    'size' => 28,
                ],
                'range' => [
                    'px' => [
                        'min' => 5,
                        'max' => 500,
                    ],
                    'em' => [
                        'min' => 5,
                        'max' => 500
                    ]
                ],
                'selectors' => [
                    '{{WRAPPER}} .title' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_responsive_control(
            'title_color',
            [
                'label' => __( 'Color', 'hornbill-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#333333',
                'selectors' => [
                    '{{WRAPPER}} .title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'title_text_transform',
            [
                'label' => __( 'Text Transform', 'hornbill-core' ),
                'type' => Controls_Manager::SELECT,
                'default' => '',
                'options' => [
                    '' => __( 'None', 'hornbill-core' ),
                    'uppercase' => __( 'UPPERCASE', 'hornbill-core' ),
                    'lowercase' => __( 'lowercase', 'hornbill-core' ),
                    'capitalize' => __( 'Capitalize', 'hornbill-core' ),
                ],
                'selectors' => [
                    '{{WRAPPER}} .title' => 'text-transform: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_section();

        // ----------- Desc Style -----------
        $this->start_controls_section(
            'style_desc_tab',
            [
                'label' => __( 'Description', 'hornbill-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_responsive_control(
            'desc_font_size',
            [
                'label' => __( 'Font Size', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px','em'],
                'default' => [
                    'unit' => 'px',
                    'size' => 16,
                ],
                'range' => [
                    'px' => [
                        'min' => 5,
                        'max' => 500,
                    ],
                    'em' => [
                        'min' => 5,
                        'max' => 500
                    ]
                ],
                'selectors' => [
                    '{{WRAPPER}} .desc' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'desc_line_height',
            [
                'label' => __( 'Font Size', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px','em'],
                'default' => [
                    'unit' => 'px',
                    'size' => 26,
                ],
                'range' => [
                    'px' => [
                        'min' => 5,
                        'max' => 500,
                    ],
                    'em' => [
                        'min' => 5,
                        'max' => 500
                    ]
                ],
                'selectors' => [
                    '{{WRAPPER}} .desc' => 'line-height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_responsive_control(
            'desc_color',
            [
                'label' => __( 'Color', 'hornbill-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#787878',
                'selectors' => [
                    '{{WRAPPER}} .desc' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'desc_text_transform',
            [
                'label' => __( 'Text Transform', 'hornbill-core' ),
                'type' => Controls_Manager::SELECT,
                'default' => '',
                'options' => [
                    '' => __( 'None', 'hornbill-core' ),
                    'uppercase' => __( 'UPPERCASE', 'hornbill-core' ),
                    'lowercase' => __( 'lowercase', 'hornbill-core' ),
                    'capitalize' => __( 'Capitalize', 'hornbill-core' ),
                ],
                'selectors' => [
                    '{{WRAPPER}} .desc' => 'text-transform: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_section();

        // ----------- Box Style -----------
        $this->start_controls_section(
            'style_box_tab',
            [
                'label' => __( 'Service Box', 'hornbill-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'bg_color',
            [
                'label' => __( 'Background Color', 'hornbill-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#FFFFFF',
                'selectors' => [
                    '{{WRAPPER}} .single-service' => 'background-color: {{VALUE}};',
                ],
            ]
        );
        $this->add_control(
            'border_color',
            [
                'label' => __( 'Hover border bottom color', 'hornbill-core' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#7f67f3',
                'selectors' => [
                    '{{WRAPPER}} .single-service:hover' => 'border-color: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_section();
    }

    /**
     * Render the widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function render() {
        $settings = $this->get_settings_for_display();
        $type = $settings['type'];
    ?>
        <div class="single-service <?php echo $type; ?>">
            <?php if($settings['image']): ?>
            <div class="icon">
                <img src="<?php echo $settings['image']['url']; ?>" class="img-fluid">
            </div>
            <?php endif; ?>
            <h3 class="title"><?php echo $settings['title'] ?></h3>
            <p class="desc"><?php echo $settings['desc'] ?></p>
        </div>
    <?php }

    /**
     * Render the widget output in the editor.
     *
     * Written as a Backbone JavaScript template and used to generate the live preview.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _content_template() { ?>
        <#
            var type = settings.type;
        #>
        <div class="single-service {{ type }}">
            <# if(settings.image.url) { #>
            <div class="icon">
                <img src="{{ settings.image.url }}" class="img-fluid">
            </div>
            <# } #>
            <h3 class="title">{{{ settings.title }}}</h3>
            <p class="desc">{{{ settings.desc }}}</p>
        </div>
    <?php }
}