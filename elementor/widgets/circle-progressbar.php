<?php
namespace HornbillElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class Circle_Progressbar extends Widget_Base {

    /**
     * Retrieve the widget name.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget name.
     */
    public function get_name() {
        return 'circle_progressbar';
    }

    /**
     * Retrieve the widget title.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget title.
     */
    public function get_title() {
        return __( 'Circle Progressbar', 'hornbill-core' );
    }

    /**
     * Retrieve the widget icon.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget icon.
     */
    public function get_icon() {
        return 'fa fa-circle-thin';
    }

    /**
     * Retrieve the list of categories the widget belongs to.
     *
     * Used to determine where to display the widget in the editor.
     *
     * Note that currently Elementor supports only one category.
     * When multiple categories passed, Elementor uses the first one.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return array Widget categories.
     */
    public function get_categories() {
        return [ 'hornbill' ];
    }

    /**
     * Register the widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _register_controls() {

        // Contents
        $this->start_controls_section(
            'section_content',
            [
                'label' => __( 'Widget Contents', 'hornbill-core' ),
            ]
        );

        $this->add_control( 'title' , [
            'label' => __('Circle Title' , 'hornbill-core'),
            'type' => Controls_Manager::TEXT,
            'label_block' => true,
            'default' => __('Circle Title' , 'hornbill-core')
        ] );

        $this->add_control(
            'percentage',
            [
                'label' => __( 'Percentage', 'plugin-domain' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ '%' ],
                'range' => [
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => '%',
                    'size' => 50,
                ]
            ]
        );

        $this->end_controls_section();


        // ---------------------------------------
        // STyle Tab
        // ---------------------------------------

        // Title

        $this->start_controls_section(
            'style_title',
            [
                'label' => __( 'Title', 'hornbill-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'title_font_size',
            [
                'label' => __( 'Font Size', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px','em'],
                'default' => [
                    'unit' => 'px',
                    'size' => 45,
                ],
                'range' => [
                    'px' => [
                        'min' => 5,
                        'max' => 500,
                    ],
                    'em' => [
                        'min' => .1,
                        'max' => 10
                    ]
                ],
                'selectors' => [
                    '{{WRAPPER}} .title' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control( 'title_color' , [
            'label' => __('Color' , 'hornbill-core'),
            'type' => Controls_Manager::COLOR,
            'default' => '#7f67f3',
            'selectors' => [
                '{{WRAPPER}} .title' => 'color: {{VALUE}};',
            ],
        ] );


        $this->end_controls_section();


        // Desc
        $this->start_controls_section(
            'style_desc',
            [
                'label' => __( 'Description', 'hornbill-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'desc_font_size',
            [
                'label' => __( 'Font Size', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px','em'],
                'default' => [
                    'unit' => 'px',
                    'size' => 16,
                ],
                'range' => [
                    'px' => [
                        'min' => 5,
                        'max' => 500,
                    ],
                    'em' => [
                        'min' => .1,
                        'max' => 10
                    ]
                ],
                'selectors' => [
                    '{{WRAPPER}} .desc' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );


        $this->add_responsive_control(
            'desc_line_height',
            [
                'label' => __( 'Line height', 'hornbill-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px','em'],
                'default' => [
                    'unit' => 'px',
                    'size' => 26,
                ],
                'range' => [
                    'px' => [
                        'min' => 5,
                        'max' => 500,
                    ],
                    'em' => [
                        'min' => .1,
                        'max' => 10
                    ]
                ],
                'selectors' => [
                    '{{WRAPPER}} .desc' => 'line-height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );


        $this->add_responsive_control( 'desc_color' , [
            'label' => __('Color' , 'hornbill-core'),
            'type' => Controls_Manager::COLOR,
            'default' => '#787878',
            'selectors' => [
                '{{WRAPPER}} .desc' => 'color: {{VALUE}};',
            ],
        ] );
        $this->end_controls_section();

    }

    /**
     * Render the widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function render() {
        $settings = $this->get_settings_for_display();
        ?>
        <div class="circle first" data-value="0.85">
            <strong></strong>
            <p><?php echo $settings['title']; ?></p>
        </div>
    <?php }

    /**
     * Render the widget output in the editor.
     *
     * Written as a Backbone JavaScript template and used to generate the live preview.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _content_template() { ?>
        <div class="circle first" data-value="0.85">
            <strong></strong>
            <p>{{ settings.title }}</p>
        </div>
    <?php }
}