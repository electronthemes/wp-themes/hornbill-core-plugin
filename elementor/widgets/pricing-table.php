<?php
namespace HornbillElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class Pricing_Table extends Widget_Base {

    /**
     * Retrieve the widget name.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget name.
     */
    public function get_name() {
        return 'pricing-table';
    }

    /**
     * Retrieve the widget title.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget title.
     */
    public function get_title() {
        return __( 'Pricing Table', 'hornbill-core' );
    }

    /**
     * Retrieve the widget icon.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget icon.
     */
    public function get_icon() {
        return 'fa fa-usd';
    }

    /**
     * Retrieve the list of categories the widget belongs to.
     *
     * Used to determine where to display the widget in the editor.
     *
     * Note that currently Elementor supports only one category.
     * When multiple categories passed, Elementor uses the first one.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return array Widget categories.
     */
    public function get_categories() {
        return [ 'hornbill' ];
    }

    /**
     * Register the widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _register_controls() {
        $this->start_controls_section(
            'section_content',
            [
                'label' => __( 'Pricing Table Contents', 'hornbill-core' ),
            ]
        );

        $this->add_control(
            'package_name',
            [
                'label' => __( 'Package Name', 'hornbill-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( 'Unlimited', 'hornbill-core' ),
                'label_block' => true
            ]
        );

        $this->add_control(
            'package_price',
            [
                'label' => __( 'Price', 'hornbill-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => __( 99 , 'hornbill-core' ),
            ]
        );

        $this->add_control(
            'package_currency_sign',
            [
                'label' => __( 'Currency Sign', 'hornbill-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( '$' , 'hornbill-core' ),
            ]
        );

        $this->add_control(
            'package_month',
            [
                'label' => __( '/Month', 'hornbill-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( 'mo' , 'hornbill-core' ),
            ]
        );

//        $this->add_control(
//            'table_style',
//            [
//                'label' => __( 'Pricing Table Style', 'hornbill-core' ),
//                'type' => Controls_Manager::SELECT,
//                'options' => [
//                    'style-one' => __('Style One' , 'hornbill-core'),
//                    'style-two' => __('Style One' , 'hornbill-core'),
//                ],
//                'default' => __('style-one' , 'hornbill-core')
//            ]
//        );
        $this->add_control(
            'gradient',
            [
                'label' => __( 'Select Gradient', 'hornbill-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'acid' => __('acid' , 'hornbill-core'),
                    'beach' => __('beach' , 'hornbill-core'),
                    'damn' => __('damn' , 'hornbill-core'),
                    'darker' => __('darker' , 'hornbill-core'),
                    'grass' => __('grass' , 'hornbill-core'),
                    'gray' => __('gray' , 'hornbill-core'),
                    'ripe' => __('ripe' , 'hornbill-core'),
                    'sharp' => __('sharp' , 'hornbill-core'),
                    'sky' => __('sky' , 'hornbill-core'),
                    'splash' => __('splash' , 'hornbill-core'),
                    'sunshine' => __('sunshine' , 'hornbill-core'),
                    'victoria' => __('victoria' , 'hornbill-core'),
                    'wine' => __('wine' , 'hornbill-core')
                ],
                'default' => 'damn'
            ]
        );

        $this->add_control(
            'icon',
            [
                'label' => __( 'Icon', 'hornbill-core' ),
                'type' => Controls_Manager::SELECT2,
                'description' => "List of all <a href='http://bicon.lab.themebucket.net/' target='_blank'>bi icons</a>",
                'options' => BI_ICONS,
                'default' => 'bi-globe3'
            ]
        );

        $this->add_control(
            'featured',
            [
                'label' => __( 'Featured', 'hornbill-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Yes' , 'hornbill-core' ),
                'label_off' => __( 'No' , 'hornbill-core' ),
                'return_value' => 'yes',
                'default' => '',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_button',
            [
                'label' => __( 'Button', 'hornbill-core' ),
            ]
        );

        $this->add_control(
            'show_button',
            [
                'label' => __( 'Show button', 'hornbill-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show' , 'hornbill-core' ),
                'label_off' => __( 'Hide' , 'hornbill-core' ),
                'return_value' => 'show',
                'default' => 'show',
            ]
        );
        $this->add_control(
            'button_text',
            [
                'label' => __( 'Text', 'hornbill-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => __('Sign Up' , 'hornbill-core'),
                'condition' => [
                    'show_button' => 'show'
                ]
            ]
        );

        $this->add_control(
            'button_url',
            [
                'label' => __( 'Url', 'hornbill-core' ),
                'type' => Controls_Manager::URL,
                'condition' => [
                     'show_button' => 'show'
                ]
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'pricing_properties_section',
            [
                'label' => __( 'Pricing Table Properties', 'hornbill-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'pricing_property', [
                'label' => __( 'Pricing Property', 'hornbill-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'pricing_properties',
            [
                'label' => __( 'Pricing Table Properties', 'hornbill-core' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'pricing_property' => __( '6 GB storage', 'hornbill-core' )
                    ],
                    [
                        'pricing_property' => __( 'Fast Brandwidht', 'hornbill-core' )
                    ],
                    [
                        'pricing_property' => __( 'Responsive', 'hornbill-core' )
                    ],
                    [
                        'pricing_property' => __( 'Customization', 'hornbill-core' )
                    ]
                ],
                'title_field' => '{{{ pricing_property }}}',
            ]
        );
        $this->end_controls_section();
    }

    /**
     * Render the widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function render() {
        $settings = $this->get_settings_for_display();

        $target = $settings['button_url']['is_external'] ? ' target="_blank"' : '';
        $nofollow = $settings['button_url']['nofollow'] ? ' rel="nofollow"' : '';
    ?>
        <div class="single-price style-two <?php echo $settings['featured'] === 'yes' ? 'middle' : '';  ?> text-white bg-<?php echo $settings['gradient']; ?>">
            <div class="icon">
                <span class="bi <?php echo $settings['icon']; ?>"></span>
            </div>
            <h4 class="package-name"><?php echo $settings['package_name']; ?></h4>
            <div class="price">
                <span class="one"><?php echo $settings['package_currency_sign']; ?></span>
                <span class="two"><?php echo $settings['package_price']; ?></span>
                <span class="three">/<?php echo $settings['package_month']; ?></span>
            </div>
            <ul class="pricing-list">
                <?php foreach ($settings['pricing_properties'] as $property): ?>
                <li><p><?php echo $property['pricing_property'] ?></p></li>
                <?php endforeach; ?>
            </ul>
            <?php if($settings['show_button']): ?>
            <a <?php echo $target . $nofollow; ?> href="<?php echo $settings['button_url']['url']; ?>" class="btn btn-circle hover-glass">
                <?php echo $settings['button_text']; ?>
            </a>
            <?php endif; ?>
        </div>
    <?php }

    /**
     * Render the widget output in the editor.
     *
     * Written as a Backbone JavaScript template and used to generate the live preview.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _content_template() { ?>
        <#
            var middle = settings.featured == 'yes' ? 'middle' : '';
            var target = settings.button_url.is_external ? ' target="_blank"' : '';
            var nofollow = settings.button_url.nofollow ? ' rel="nofollow"' : '';
        #>
        <div class="single-price style-two text-white bg-{{{ settings.gradient }}} {{{ middle }}}">
            <div class="icon">
                <span class="bi {{{ settings.icon }}}"></span>
            </div>
            <h4 class="package-name">{{{ settings.package_name }}}</h4>
            <div class="price">
                <span class="one">{{{ settings.package_currency_sign }}}</span>
                <span class="two">{{{ settings.package_price }}}</span>
                <span class="three">/{{{ settings.package_month }}}</span>
            </div>
            <ul class="pricing-list">
                <# _.each( settings.pricing_properties, function( property ) { #>
                    <li><p>{{ property.pricing_property }}</p></li>
                <# } ); #>
            </ul>
            <# if(settings.show_button){ #>
            <a href="{{ settings.button_url.url }}" class="btn btn-circle hover-glass" {{ target }} {{ nofollow }}>
                {{ settings.button_text }}
            </a>
            <# } #>
        </div>
    <?php }
}